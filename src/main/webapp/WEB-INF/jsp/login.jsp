<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

        <style>
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #eee;
            }

            .form-signin {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin .checkbox {
                font-weight: normal;
            }
            .form-signin .form-control {
                position: relative;
                height: auto;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                padding: 10px;
                font-size: 16px;
            }
            .form-signin .form-control:focus {
                z-index: 2;
            }
            .form-signin input[type="email"] {
                margin-bottom: -1px;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
            }
            .form-signin input[type="password"] {
                margin-bottom: 10px;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
        </style>

    </head>
    <body>
        <div class="container">

            <c:if test="${not empty param.authentication_error}">
                    <h1>Woops!</h1>

                    <p class="error">Your login attempt was not successful.</p>
            </c:if>
            <c:if test="${not empty param.authorization_error}">
                    <h1>Woops!</h1>

                    <p class="error">You are not permitted to access that resource.</p>
            </c:if>
            
                        
            <% if (request.getParameter("error") != null) {%>
            <div class="alert alert-danger">${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</div>
            <% }%>
            
            <form action="<c:url value="/login.do"/>" method="post" role="form">
                     <fieldset>
                             <legend>
                                     <h2>Login</h2>
                             </legend>
                             <div class="form-group">
                                     <label for="username">Username:</label> <input id="username"
                                             class="form-control" type='text' name='j_username'/>
                             </div>
                             <div class="form-group">
                                     <label for="">Password:</label> <input id="password"
                                             class="form-control" type='text' name='j_password'/>
                             </div>
                             <button class="btn btn-primary" type="submit">Login</button>
                             <input type="hidden" name="${_csrf.parameterName}"
                                     value="${_csrf.token}" />
                     </fieldset>
             </form>
             
             <a href="${pageContext.request.contextPath}/logout.do">Logout</a>

        </div> <!-- /container -->
    </body>
</html>
