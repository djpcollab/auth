<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>

        <!-- Latest compiled and minified CSS -->
        <!--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">-->

        <!-- Optional theme -->
        <!--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">-->

        <style>
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #eee;
            }

            .form-signin {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin .checkbox {
                font-weight: normal;
            }
            .form-signin .form-control {
                position: relative;
                height: auto;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                padding: 10px;
                font-size: 16px;
            }
            .form-signin .form-control:focus {
                z-index: 2;
            }
            .form-signin input[type="email"] {
                margin-bottom: -1px;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
            }
            .form-signin input[type="password"] {
                margin-bottom: 10px;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
        </style>

    </head>
    <body>
        <div class="container">

            <% if (request.getParameter("error") != null) {%>
            <div class="alert alert-danger">${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</div>
            <% }%>
            <!--         action="/login.do"   <form class="form-signin" role="form" action="j_spring_security_check" method="post">-->
            <!--            <form class="form-signin" role="form" action="j_spring_security_check"  method="post">
                            <h2 class="form-signin-heading">Silahkan ffsdfs  Login</h2>
                            <input type="text" name="j_username" class="form-control" placeholder="Username" required autofocus>
                            <input type="password" name="j_password" class="form-control" placeholder="Password" required>
                                            <label class="checkbox">
                                                <input type="checkbox" value="remember-me"> Remember me
                                            </label>
                            <input type="hidden" name="spring-security-redirect" value="http://google.com">
                            <input type="hidden"
                                   name="${_csrf.parameterName}"
                                   value="${_csrf.token}"/>
            
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                        </form>-->
            <form class="login-form" action="j_spring_security_check" method="POST">
                <h3 class="form-title">Gunakan akun SIKKA Anda </h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span>masukkan username dan password yang sesuai</span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="j_username" id="txtNip"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="j_password" id="txtPasswd"/>
                    </div>
                </div>
                <div class="form-actions">
                    <!--                    <label class="checkbox">
                                            <input type="checkbox" name="remember" value="1"/> Ingat di komputer ini
                                        </label>-->
                    <button type="submit" id="btnLogin" class="btn green pull-right">
                        Login <i class="m-icon-swapright m-icon-white"></i>
                    </button>            
                </div>

            </form>

        </div> <!-- /container -->
    </body>
</html>
