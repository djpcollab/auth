/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package authserver.auth;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
//import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;


//import org.springframework.security.oauth2.provider.token.InMemoryTokenStore;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 *
 * @author TTKI
 */
public class LogoutImpl implements LogoutSuccessHandler {

    //InMemoryTokenStore tokenstore;
    JdbcTokenStore tokenstore;
    
    public JdbcTokenStore getTokenstore() {
        return tokenstore;
    }

    public void setTokenstore(JdbcTokenStore tokenstore) {
        this.tokenstore = tokenstore;
    }

//    public InMemoryTokenStore getTokenstore() {
//        return tokenstore;
//    }
//
//    public void setTokenstore(InMemoryTokenStore tokenstore) {
//        this.tokenstore = tokenstore;
//    }
    
    

    @Override
    public void onLogoutSuccess(HttpServletRequest hsr, HttpServletResponse hsr1, Authentication a) throws IOException, ServletException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        removeaccess(hsr);
        hsr1.getOutputStream().write("\n\tYou Have Logged Out successfully.".getBytes());
    }

    public void removeaccess(HttpServletRequest req) {

        //String tokens = req.getHeader("Authorization");
        String tokens = req.getParameter("token");
        System.out.println(tokens);
        //String value = tokens.substring(tokens.indexOf(" ")).trim();
        DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(tokens);
        System.out.println(token);
        tokenstore.removeAccessToken(tokens);
        System.out.println("\n\tAccess Token Removed Successfully!!!!!!!!");

    }

    
}
