/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package authserver.auth;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.stereotype.Service;

/**
 *
 * @author TTKI
 */
@Service
public class ClientDetailsServiceImpl implements ClientDetailsService {

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws OAuth2Exception {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<String> authorizedGrantTypes = new ArrayList<>();
        BaseClientDetails clientDetails = new BaseClientDetails();
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        Set<String> uri = new HashSet<>();

        switch (clientId) {
            case "client1":
                authorizedGrantTypes.add("password");
                authorizedGrantTypes.add("refresh_token");
                authorizedGrantTypes.add("client_credential");
                //authorizedGrantTypes.add("implicit");
                clientDetails.setClientId("client1");
                clientDetails.setClientSecret("client1");
                clientDetails.setAuthorizedGrantTypes(authorizedGrantTypes);
                //clientDetails.setRegisteredRedirectUri("");
                //uri.add("http://localhost:34301/ClientAuth1/index.htm");
                //grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));                
                //clientDetails.setRegisteredRedirectUri(uri);
                clientDetails.setAuthorities(grantedAuthorities);
                //clientDetails.addAdditionalInformation("ahdhashd", "lkjkl");
                return clientDetails;

            case "client2":
                authorizedGrantTypes.add("password");
                authorizedGrantTypes.add("refresh_token");
                authorizedGrantTypes.add("client_credentials");
                clientDetails.setClientId("client2");
                clientDetails.setClientSecret("client2");
                clientDetails.setAuthorizedGrantTypes(authorizedGrantTypes);
                return clientDetails;

            case "client3":
                //authorizedGrantTypes.add("password");
                //authorizedGrantTypes.add("refresh_token");
                //authorizedGrantTypes.add("client_credentials");
                authorizedGrantTypes.add("implicit");
                clientDetails.setClientId("client3");
                clientDetails.setClientSecret("client3");
                clientDetails.setAuthorizedGrantTypes(authorizedGrantTypes);
                uri.add("http://spnapp1");
                clientDetails.setRegisteredRedirectUri(uri);
                return clientDetails;

            default:
                throw new NoSuchClientException("No client with requested id: "
                        + clientId);
        }
    }
}
