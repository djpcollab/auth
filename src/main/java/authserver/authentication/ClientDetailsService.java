/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package authserver.authentication;

import java.util.ArrayList;
import java.util.List;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

/**
 *
 * @author TTKI
 */
@Service("clientDetailsUserService")
public class ClientDetailsService implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String name = a.getName();
        String password = a.getCredentials().toString();


        List<String> roles = getRoleOffline("Super");


        List< GrantedAuthority> grantedAuths = new ArrayList<>();
        for (int i = 0;
                i < roles.size();
                i++) {
            grantedAuths.add(new SimpleGrantedAuthority(roles.get(i).toString()));
        }
        Authentication auth = new UsernamePasswordAuthenticationToken("tenan", password, grantedAuths);
        return auth;
    }

    @Override
    public boolean supports(Class<?> type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private List<String> getRoleOffline(String roleName) {
        List<String> roles = new ArrayList<>();
        roles.add(roleName);
        return roles;
    }
}
