/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package authserver.authentication;

/**
 *
 * @author TTKI
 */
public class UserSikka {

    String nm_peg;
    String kd_kantor;
    String kode_kanwil;
    String kode_kpp;
    String nm_kantor;
    String nm_unit_org;
    String kd_unit_org;
    String jns_jabatan;
    String kd_jabatan;
    String nm_jabatan;
    String nip;
    String jns_kantor;
    String kd_jns_kantor;
    String url_photo;
    String nip_baru;

    public String getNm_peg() {
        return nm_peg;
    }

    public void setNm_peg(String nm_peg) {
        this.nm_peg = nm_peg;
    }

    public String getKd_kantor() {
        return kd_kantor;
    }

    public void setKd_kantor(String kd_kantor) {
        this.kd_kantor = kd_kantor;
    }

    public String getKode_kanwil() {
        return kode_kanwil;
    }

    public void setKode_kanwil(String kode_kanwil) {
        this.kode_kanwil = kode_kanwil;
    }

    public String getKode_kpp() {
        return kode_kpp;
    }

    public void setKode_kpp(String kode_kpp) {
        this.kode_kpp = kode_kpp;
    }

    public String getNm_kantor() {
        return nm_kantor;
    }

    public void setNm_kantor(String nm_kantor) {
        this.nm_kantor = nm_kantor;
    }

    public String getNm_unit_org() {
        return nm_unit_org;
    }

    public void setNm_unit_org(String nm_unit_org) {
        this.nm_unit_org = nm_unit_org;
    }

    public String getKd_unit_org() {
        return kd_unit_org;
    }

    public void setKd_unit_org(String kd_unit_org) {
        this.kd_unit_org = kd_unit_org;
    }

    public String getJns_jabatan() {
        return jns_jabatan;
    }

    public void setJns_jabatan(String jns_jabatan) {
        this.jns_jabatan = jns_jabatan;
    }

    public String getKd_jabatan() {
        return kd_jabatan;
    }

    public void setKd_jabatan(String kd_jabatan) {
        this.kd_jabatan = kd_jabatan;
    }

    public String getNm_jabatan() {
        return nm_jabatan;
    }

    public void setNm_jabatan(String nm_jabatan) {
        this.nm_jabatan = nm_jabatan;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getJns_kantor() {
        return jns_kantor;
    }

    public void setJns_kantor(String jns_kantor) {
        this.jns_kantor = jns_kantor;
    }

    public String getKd_jns_kantor() {
        return kd_jns_kantor;
    }

    public void setKd_jns_kantor(String kd_jns_kantor) {
        this.kd_jns_kantor = kd_jns_kantor;
    }

    public String getUrl_photo() {
        return url_photo;
    }

    public void setUrl_photo(String url_photo) {
        this.url_photo = url_photo;
    }

    public String getNip_baru() {
        return nip_baru;
    }

    public void setNip_baru(String nip_baru) {
        this.nip_baru = nip_baru;
    }

    
}
