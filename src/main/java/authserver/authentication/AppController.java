/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package authserver.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import authserver.dao.ResourceDAO;
import authserver.entity.EmployeeEntity;
import authserver.entity.RefreshToken;
import authserver.entity.TokenEntity;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author TTKI
 */
@Controller
public class AppController {
    @Autowired
    private ResourceDAO resource;

    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
    public String listEmployees(ModelMap map)
    {
        map.addAttribute("employee", "employee");
        return "login";
    }
    
    @RequestMapping(value = "test/ateam", method = RequestMethod.GET)
    public String test() {
        return "test";
    }

    @RequestMapping(value = "admin/test", method = RequestMethod.GET)
    public String www() {
        return "admin";
    }

    @RequestMapping(value = "staff/test", method = RequestMethod.GET)
    public String fsdfs() {
        return "staff";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String fsdfsd() {
        return "login";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String fssdfdfsd() {
        return "index";
    }
    
    @RequestMapping(value = "/insertemployee", method = RequestMethod.GET)
    public String insertEmployee(){
        EmployeeEntity empl = new EmployeeEntity();
        empl.setFirstname("First");
        empl.setLastname("Last");
        empl.setEmail("Email");
        empl.setTelephone("tel");
        resource.addEmployee(empl);
        return "index";
    }
    
    @RequestMapping(value="/getjsession", method = RequestMethod.GET)
    @ResponseBody
    public String getJsession(HttpServletRequest request){
        HttpSession session = request.getSession();
        
        return session.getId();
    }
    
    @RequestMapping(value = "/sendtoken", method = {RequestMethod.GET,RequestMethod.POST})
    @ResponseBody
    public TokenEntity sendToken(){
        TokenEntity token = new TokenEntity();
        List<TokenEntity> tokenList = resource.getTokenEntityByUsername("user1");
        if(!tokenList.isEmpty()){
            token = tokenList.get(0);
//            Base64 encoder = new Base64();
            String authentication = Base64.encodeBase64String(token.getAuthentication());//encoder.encodeToString(token.getAuthentication());
            String tokens = Base64.encodeBase64String(token.getToken());//encoder.encodeToString(token.getToken());
            
            /**
             * kirim access token ke resource server
             */
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getForObject("http://localhost:9292/RestServer/addtoken?token_id={token_id}&token={token}&authentication_id={authentication_id}&user_name={user_name}&client_id={client_id}&authentication={authentication}&refresh_token={refresh_token}", TokenEntity.class, token.getToken_id(), tokens, token.getAuthentication_id(), token.getUser_name(), token.getClient_id(), authentication, token.getRefresh_token());
            
            /**
             * kirim refresh token ke resource server
             */
            List<RefreshToken> refreshTokenList = resource.getRefreshTokenEntityByTokenId(token.getRefresh_token());
            RefreshToken refreshToken = refreshTokenList.get(0);
            String authenticationRefresh = Base64.encodeBase64String(refreshToken.getAuthentication());
            String tokensRefresh = Base64.encodeBase64String(refreshToken.getToken());
            restTemplate.getForObject("http://localhost:9292/RestServer/addrefreshtoken?token_id={token_id}&token={token}&authentication={authentication}", RefreshToken.class, refreshToken.getToken_id(), tokensRefresh, authenticationRefresh);
        }else{
            
        }
        return token;
    }
}
