/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authserver.dao;

/**
 *
 * @author kuroshaki
 */
import java.util.List;
import authserver.entity.EmployeeEntity;
import authserver.entity.RefreshToken;
import authserver.entity.TokenEntity;

 
public interface ResourceDAO
{
    public void addEmployee(EmployeeEntity employee);
    public List<EmployeeEntity> getAllEmployees();
    public void deleteEmployee(Integer employeeId);
    public List<EmployeeEntity> getEmployeeEntityById(Integer employeeId);
    public void addToken(TokenEntity token);
    public List<TokenEntity> getTokenEntityByUsername(String username);
    public void addRefreshToken(RefreshToken refreshtoken);
    public List<RefreshToken> getRefreshTokenEntityByTokenId(String tokenid);
    
}
