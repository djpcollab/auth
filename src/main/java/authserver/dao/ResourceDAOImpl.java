/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authserver.dao;

/**
 *
 * @author kuroshaki
 */
import java.util.List;
 
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import authserver.entity.EmployeeEntity;
import authserver.entity.RefreshToken;
import authserver.entity.TokenEntity;
import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;
 
@Repository
public class ResourceDAOImpl implements ResourceDAO
{
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    @Transactional
    public void addEmployee(EmployeeEntity employee) {
        this.sessionFactory.getCurrentSession().save(employee);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<EmployeeEntity> getAllEmployees() {
        return this.sessionFactory.getCurrentSession().createQuery("from EmployeeEntity ").list();
    }
    
    @Override
    @Transactional
    public void deleteEmployee(Integer employeeId) {
        EmployeeEntity employee = (EmployeeEntity) sessionFactory.getCurrentSession().load(
                EmployeeEntity.class, employeeId);
        if (null != employee) {
            this.sessionFactory.getCurrentSession().delete(employee);
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<EmployeeEntity> getEmployeeEntityById(Integer employeeId) {
        String hql = "from EmployeeEntity where ID ="+employeeId;
        return this.sessionFactory.getCurrentSession().createQuery(hql).list();
    }
    
    @Override
    @Transactional
    public void addToken(TokenEntity token) {
        this.sessionFactory.getCurrentSession().save(token);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<TokenEntity> getTokenEntityByUsername(String username) {
        String hql = "from TokenEntity where user_name = :username";
        Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("username", username);
        return query.list();
    }
    
    @Override
    @Transactional
    public void addRefreshToken(RefreshToken refreshtoken) {
        this.sessionFactory.getCurrentSession().save(refreshtoken);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<RefreshToken> getRefreshTokenEntityByTokenId(String tokenid) {
        String hql = "from RefreshToken where token_id = :token_id";
        Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("token_id", tokenid);
        return query.list();
    }
    
}
