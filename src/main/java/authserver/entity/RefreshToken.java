/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authserver.entity;

/**
 *
 * @author kuroshaki
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.codehaus.jackson.annotate.JsonAutoDetect;


@Entity
@Table(name="oauth_refresh_token")
@JsonAutoDetect
public class RefreshToken {
    @Id
    @Column(name="token_id")
    private String token_id;
    @Column(name="token")
    private byte[] token;
    @Column(name="authentication")
    private byte[] authentication;
    
    
    public RefreshToken(){
        
    }
    
    /**
     * Getter and Setter
     */
    
    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public byte[] getToken() {
        return token;
    }

    public void setToken(byte[] token) {
        this.token = token;
    }

    public byte[] getAuthentication() {
        return authentication;
    }

    public void setAuthentication(byte[] authentication) {
        this.authentication = authentication;
    }

    
    
}
