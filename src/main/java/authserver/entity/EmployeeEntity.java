/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authserver.entity;

/**
 *
 * @author kuroshaki
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.codehaus.jackson.annotate.JsonAutoDetect;


@Entity
@Table(name="employee")
@JsonAutoDetect
public class EmployeeEntity {
    @Id
    @Column(name="id")
    @GeneratedValue
    private Integer id;
    @Column(name="FIRSTNAME")
    private String firstname;
    @Column(name="LASTNAME")
    private String lastname;
    @Column(name="EMAIL")
    private String email;
    @Column(name="TELEPHONE")
    private String telephone;
    
    
    public EmployeeEntity(Integer id, String first, String last, String email, String tel){
        this.id = id;
        this.firstname = first;
        this.lastname = last;
        this.email = email;
        this.telephone = tel;
    }
    
    public EmployeeEntity(){
        
    }
    
    /*
     * Setters and getters
     */
    
    public String getEmail() {
        return email;
    }
    public String getTelephone() {
        return telephone;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    public String getFirstname() {
        return firstname;
    }
    public String getLastname() {
        return lastname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
}
